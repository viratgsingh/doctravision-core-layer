require('dotenv').config({ path: __dirname + '/.env'});
const express = require('express');
const mongoose = require('mongoose');
var userRoute = require('./routes/userRoute');
var publisherRoute = require('./routes/publisherRoute');
var sCategoryRoute = require('./routes/sCategoryRoute');
var tagRoute = require('./routes/tagRoute');
var adminRoute = require('./routes/adminRoute');
var videoRoute = require('./routes/videoRoute');
var categoryRoute = require('./routes/categoryRoute');
var salesleadsRoute = require('./routes/salesleadsRoute');
var notificationsRoute = require('./routes/notificationsRoute');


const { genericErrorHandler, unknownRoutesHandler } = require('./handlers/error/errorHandler');

var app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//DEV routes
app.use('/core/user', userRoute);
app.use('/core/publisher', publisherRoute);
app.use('/core/category',categoryRoute);
app.use('/core/sCategory',sCategoryRoute);
app.use('/core/video', videoRoute);
app.use('/core/admin', adminRoute);
app.use('/core/tag', tagRoute);
app.use('/core/saleslead', salesleadsRoute);
app.use('/core/notification', notificationsRoute);

// this matches all routes and all methods
app.use(unknownRoutesHandler);
//Custom error handler. Always define at last
app.use(genericErrorHandler);


//mongo connection
mongoose.connect(process.env.MONGO_URI, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
});
mongoose.promise = global.Promise;
const mongo = mongoose.connection;
mongo.on('error', error => { console.log('mongo: ' + error.name); });
mongo.on('connected', async () => { console.log('mongo: Connected'); });
mongo.on('disconnected', () => { console.log('mongo: Disconnected'); });

let port = process.env.NODE_PORT;
app.listen(port);
console.log(`Server started at port ${port}`);
