module.exports = {
    RESPONSE_STATUS: {
        STATUS_SUCCESS: 'SUCCESS',
        STATUS_FAIL: 'FAIL'
    },
    ERRORS : {
        INVALID_GET_QUERY : "Invalid GET Query",
        INVALID_POST_DATA : "Invalid POST Data"
    }
};