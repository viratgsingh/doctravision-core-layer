const DataModel = require("../models/Publisher");

module.exports = {
	create: async (create) => {
		let data = await DataModel.create(create);
		return data;
	},

	list: async (filter) => {
		let data = await DataModel.find(filter).exec();
		return data;
	},

	get: async (filter) => {
		let data = await DataModel.findOne(filter).exec();
		return data;
	},

	update: async (filter, update) => {
		let data = await DataModel.updateMany(filter, update, {new: true}).exec();
		return data;
	},

	updateOne: async (filter, update) => {
		let data = await DataModel.findOneAndUpdate(filter, update, {new: true}).exec();
		return data;
	},

	delete: async (filter) => {
		let data = await DataModel.deleteMany(filter);		//This is HARD DELETE
		return data;
	},

	deleteOne: async (filter) => {
		let data = await DataModel.deleteOne(filter);		//This is HARD DELETE
		return data;
	},

	count : async(filter) => {
		let data = await DataModel.countDocuments(filter).exec();
		return data;
	},
	maxCount : async(filter) => {
		let data = await DataModel.find(filter).exec();
		return data;
	},
	search: async(filter) => {
		let searchString = filter["name"];
		let regex = new RegExp(searchString,'i');
		filter["name"]=regex
		filter["description"]=regex
		let data = await DataModel.find(filter).limit(5);
		return data;
	},
	compile: async(filter) => {
		let tags = filter.tags;
		let tagsList=tags.split(",");
		let len = tagsList.length;
		let addedtagsList = []; 
		for (i=0 ;i < len ;i++ ){
			addedtag = new RegExp(tagsList[i],'i');
			addedtagsList.push(addedtag);
		};
		filter["added_tags"]=filter["tags"];
		delete filter["tags"];
		filter["added_tags"]={ $all: addedtagsList }; 
		let data = await DataModel.find(filter).exec();
		return data;
	}
};
