const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    }
}, { 
    timestamps: true,
    toJSON: {
        transform: function (doc, ret) {
            delete ret.isDeleted;
        }
    }
});

module.exports = mongoose.model('User', userSchema);
